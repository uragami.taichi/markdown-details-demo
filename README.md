# markdown-details-demo

`details`要素を使えばアコーディオン的な見た目にできるはず。

<details>
<summary>App.java</summary>

```
public class App {

    public static void main(String[] args) {
        System.out.println("Hello, world!");
    }
}
```
</details>
